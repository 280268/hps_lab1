
#include <stdio.h>
  #include <stdlib.h>
#include <omp.h>
void insert_sort(int a[],int l)
{
    int i,temp,p;
    //从第２个元素开始
	#pragma omp parallel for 
    for (i = 1; i<l ; i++){
        temp = a[i];//将带插入元素拿出来
        p = i-1;
        while (p>=0 && temp<a[p]){
            //比较大的元素向后挪一位，腾出空间
            a[p+1] = a[p];
            p--;
        }

        //插入
        a[p+1] = temp;
    }

}

int main(int argc,char **argv)
{
	int i,iNum,nums;
	 iNum=atoi(argv[1]);
	 nums=atoi(argv[2]);
    int a[iNum];
    for(i=0;i<iNum;i++)
		a[i]=rand()%(iNum*20);
	for (i = 0; i < iNum; ++i) {
        printf("%d ",a[i]);
    } 
	printf("\n");
	omp_set_num_threads(nums);
	double tt=omp_get_wtime();
    insert_sort(a,iNum);
	double t0=omp_get_wtime();
    for (i = 0; i < iNum; ++i) {
        printf("%d ",a[i]);
    }
	printf("\n");
	  printf("Etimes %f seconds\n",t0-tt);
 
 return 0;
}
