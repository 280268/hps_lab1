#include <stdio.h>
  #include <stdlib.h>
#include <omp.h>
 typedef struct _Node
 {
      int value;
      struct _Node * pLeft;
      struct _Node * pRight;
  } Node;
  
  Node * getNewNode(int iValue)
  {
      Node * p = (Node *)malloc(sizeof(Node));
     if(NULL != p)
      {
          p->value = iValue;
          p->pLeft = NULL;
          p->pRight = NULL;
      }
  
      return p;
  }
  
  void deleteNode(Node * p)
  {
      if(NULL != p)
      {
          free(p);
      }
  }
  
  int addElm(Node * p, int value)
  {
      Node * pX = p;
      while(pX)
      {
          if(value < pX->value)
          {
              if(NULL == pX->pLeft)
              {
                  pX->pLeft = getNewNode(value);
                 
                  break;
              }
              else
              {
                  pX = pX->pLeft;
              }
          }
          else
          {
              if(NULL == pX->pRight)
              {
                  pX->pRight = getNewNode(value);
                 
                  break;
              }
              else
              {
                  pX = pX->pRight;
              }
          }
      }
  
      return 0;
  }
  
  Node * makeBinaryTree(int iList[], int iNum)
  {
      Node * p = getNewNode(iList[0]);
      int i = 0;
	  #pragma omp parallel for 
      for(i = 1; i < iNum; i++)
      {
          addElm(p, iList[i]);
      }
      printf("\n");
  
      return p;
  }
  
  void destroyBinaryTree(Node * p)
  {
      if(NULL == p)
      {
          return;
      }
      destroyBinaryTree(p->pLeft);
      destroyBinaryTree(p->pRight);
      deleteNode(p);
  }
  


 int inorderTraversal(Node * p)
 {
     if(NULL == p)
     {
         return -1;
     }
	 #pragma omp parallel
	 {
	#pragma omp single
     inorderTraversal(p->pLeft);
     printf("%2d ", p->value);
	 #pragma omp single
     inorderTraversal(p->pRight);
     }
	 return 0;
 }
 

 
 void inTrvl(Node * p)
 {
     printf("in   : ");
     inorderTraversal(p);
     printf("\n");
 }
 
 void printList(int iList[], int iNum)
 {
     for(int i = 0; i < iNum; i++)
     {
         printf("%d ", iList[i]);
     }
     printf("\n");
 }
 
 int main(int argc,char **argv)
 {
	 int i,iNum,nums;
	 iNum=atoi(argv[1]);
	 nums=atoi(argv[2]);
	 
     int iList[iNum];
     for(i=0;i<iNum;i++)
		iList[i]=rand()%(iNum*20);
	
     printList(iList, iNum);
	   omp_set_num_threads(nums);
	   double tt=omp_get_wtime();
     Node * p = makeBinaryTree(iList, iNum);
     inTrvl(p);
	 double t0=omp_get_wtime();
	  printf("Etimes %f seconds\n",t0-tt);
     destroyBinaryTree(p);
}
